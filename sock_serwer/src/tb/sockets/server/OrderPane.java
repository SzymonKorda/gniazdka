package tb.sockets.server;

import javax.swing.JPanel;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;


public class OrderPane extends JPanel {  //order pane to miejsce gdzie rysujemy
	
	/**
	 * Create the panel.
	 */
	public OrderPane() {
		super();
		setBackground(new Color(255, 255, 240));
		
	}
	
	public void Rysuj(int indeks)	//na podstawie indeksu karty rysujemy odpowiedni znak
	{
		Graphics2D g = (Graphics2D) getGraphics();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 250, 350);
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, 249, 349);
		g.setStroke(new BasicStroke(5));
		
		if(indeks == 1 || indeks == 14 || indeks == 27 || indeks == 40)
		{
			g.drawLine(50, 50, 200, 50);
			g.drawLine(200, 50, 200, 175);
			g.drawLine(200, 175, 50, 175);
			g.drawLine(50, 175, 50, 300);
			g.drawLine(50, 300, 200, 300);
		}
		if(indeks == 2 || indeks == 15 || indeks == 28 || indeks == 41)
		{
			g.drawLine(50, 50, 200, 50);
			g.drawLine(200, 50, 200, 175);
			g.drawLine(200, 175, 50, 175);
			g.drawLine(200, 175, 200, 300);
			g.drawLine(200, 300, 50, 300);
		}
		if(indeks == 3 || indeks == 16 || indeks == 29 || indeks == 42)
		{
			g.drawLine(50, 50, 50, 175);
			g.drawLine(50, 175, 200, 175);
			g.drawLine(200, 175, 200, 300);
		}
		if(indeks == 4 || indeks == 17 || indeks == 30 || indeks == 43)
		{
			g.drawLine(50, 50, 200, 50);
			g.drawLine(50, 50, 50, 175);
			g.drawLine(50, 175, 200, 175);
			g.drawLine(200, 175, 200, 300);
			g.drawLine(200, 300, 50, 300);
		}
		if(indeks == 5 || indeks == 18 || indeks == 31 || indeks == 44)
		{
			g.drawLine(50, 50, 200, 50);
			g.drawLine(50, 50, 50, 175);
			g.drawLine(50, 175, 200, 175);
			g.drawLine(200, 175, 200, 300);
			g.drawLine(50, 175, 50, 300);
			g.drawLine(200, 300, 50, 300);
		}
		if(indeks == 6 || indeks == 19 || indeks == 32 || indeks == 45)
		{
			g.drawLine(50, 50, 200, 50);
			g.drawLine(200, 50, 100, 300);
		}
		if(indeks == 7 || indeks == 20 || indeks == 33 || indeks == 46)
		{
			g.drawLine(50, 50, 200, 50);
			g.drawLine(50, 50, 50, 175);
			g.drawLine(50, 175, 200, 175);
			g.drawLine(200, 175, 200, 300);
			g.drawLine(50, 175, 50, 300);
			g.drawLine(200, 300, 50, 300);
			g.drawLine(200, 50, 200, 300);
		}
		if(indeks == 8 || indeks == 21 || indeks == 34 || indeks == 47)
		{
			g.drawLine(50, 50, 200, 50);
			g.drawLine(50, 50, 50, 175);
			g.drawLine(50, 175, 200, 175);
			g.drawLine(200, 175, 200, 300);
			g.drawLine(200, 50, 200, 300);
		}
		if(indeks == 9 || indeks == 22 || indeks == 35 || indeks == 48)
		{
			g.drawLine(10, 175, 80, 50);
			g.drawLine(80, 50, 80, 300);
			g.drawLine(100, 50, 240, 50);
			g.drawLine(240, 50, 240, 300);
			g.drawLine(100, 50, 100, 300);
			g.drawLine(240, 300, 100, 300);
		}
		if(indeks == 10 || indeks == 23 || indeks == 36 || indeks == 49)
		{
			g.drawLine(20, 50, 75, 300);
			g.drawLine(75, 300, 120, 175);
			g.drawLine(120, 175, 165, 300);
			g.drawLine(165, 300, 230, 50);
		}
		if(indeks == 11 || indeks == 24 || indeks == 37 || indeks == 50)
		{
			g.drawLine(50, 50, 50, 300);
			g.drawLine(50, 50, 200, 125);
			g.drawLine(50, 300, 200, 225);
			g.drawLine(200, 125, 200, 225);
		}
		if(indeks == 12 || indeks == 25 || indeks == 38 || indeks == 51)
		{
			g.drawLine(50, 50, 50, 300);
			g.drawLine(50, 175, 200, 50);
			g.drawLine(50, 175, 200, 300);
		}
		if(indeks == 13 || indeks == 26 || indeks == 39 || indeks == 52)
		{
			g.drawLine(50, 300, 125, 50);
			g.drawLine(125, 50, 200, 300);
			g.drawLine(84, 200, 166, 200);
		}
	}
	@Override
	protected void paintComponent(Graphics g)		//ustawienie tla i ramki
	{
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 250, 350);
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, 249, 349);
	}  
}