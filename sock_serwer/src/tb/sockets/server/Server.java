package tb.sockets.server;

import java.io.*;
import java.net.*;

import tb.sockets.server.Karta;
import tb.sockets.server.Talia;
import tb.sockets.server.MainFrame;
						//server i client to gracze, nazwy sa takie by ich latwo odroznic
public class Server 	//server i client maja talie, gdy jeden z nich dobierze karte wysylamy indeks do 
{						//drugiego i usuwamy odpowiednia karte, dzieki temu server i client korzystaja
						//praktycznie z tej samej talii, to jak sa potasowane w praktyce nie ma znaczenia
	public Talia sr_tal =  new Talia(); 
	public static int sr_pkt = 0;	//punkty servera
	public static int encl_pkt = 0;	//punkty clienta
	public int sr_asy = 0;			//liczba asow servera
	public int encl_asy = 0;		//liczba asow clienta
	public Karta sr_kar = new Karta("a", "b", 0, 0); //karta jaka ma obecnie na rece server 
	public static int sr_decyzja = 0; //obecna decyzja servera, 0 czyli brak decyzji
	static int encl_decyzja = 0;	//obecna decyzja clienta
	int pom = 0;					//zmienna pomocnicza przy wyswietlaniu wyniku przy podwojnym pasowaniu
	static boolean tura = false;    //tura false gdyz zaczyna client
	public ServerSocket sersock;
	Socket sock;
	BufferedReader keyRead;
	OutputStream ostream;
	PrintWriter pwrite;
	InputStream istream;
	static BufferedReader receiveRead;
	public Server() throws Exception	//tworzymy ServerCosket i socket oraz strumienie do wysylania i odbierania danych
	{
		sr_tal.TasujKarty();
		sersock = new ServerSocket(3000);
	    sock = sersock.accept( );                          
	    ostream = sock.getOutputStream(); 
	    pwrite = new PrintWriter(ostream, true);
	    istream = sock.getInputStream();
	    receiveRead = new BufferedReader(new InputStreamReader(istream));
	   
	}
	
	public void Rozgrywka() throws Exception
	{
	    while(true)  //nieskonczona petla
	    {
	    	pwrite.flush();
	    	if(sr_decyzja == 1) //jezeli decyzja jest 1 to dobieramy karte
	    	{
	    		if(tura == true)  //jezeli tura ma wartosc true to znaczy ze jest tura tego gracza
	    		{
	    			sr_kar = sr_tal.DobierzKarte();  //dobieramy karte 
					sr_pkt = sr_pkt + sr_kar.punkty; //zliaczmy punkty
					if(sr_kar.indeks == 13 || sr_kar.indeks == 26 || sr_kar.indeks == 39 || sr_kar.indeks == 52)
						sr_asy++;					//jezeli as to zwiekszamy ilosc asow
					MainFrame.Rysuj(sr_kar.indeks);	//rysujemy odpowiedni symbol
					MainFrame.UstawWyniks(encl_pkt, sr_pkt);	//ustawiamy punkty servera
					MainFrame.JakaKartas(sr_kar.indeks);		//wyswietlamy jaka karte wylosowalismy
					MainFrame.SprawdzWynik(encl_pkt, sr_pkt, encl_asy, sr_asy);	//sprawdzamy wynik
					
					pwrite.write(sr_kar.indeks);  //wysylamy indeks karty by usunac je z talii i przekazac punkty servera
					if(sr_kar.indeks != 53)      //indeks 53 oznacza, ze gracz spasowal
						tura = false;			//tura na false gdyz nastepnie bedziemy odbierac karte
					pwrite.flush();
					sr_decyzja = 0;				//zerujemy decyzje
	    		}
	    	}
	    	
	    	if(sr_decyzja == 2)				//decyzja o pasowaniu
	    	{
	    		
	    		if(tura == true)
	    		{
	    			if(sr_decyzja == 2 && encl_decyzja == 2)	//dwoch graczy spasowalo
					{
						MainFrame.SprawdzWynikPas(encl_pkt, sr_pkt, encl_asy, sr_asy); //sprawdzamy wynik
					}
	    			pwrite.write(53); //dajemy znac clientowi ze spasowalismy
	    			pwrite.flush();
	    			tura = false;		//tura na false gdyz biedziemy tylko odbierac
	    			MainFrame.sr_dobierz.setEnabled(false);	//wylaczamy przyciski
	    			MainFrame.sr_pasuj.setEnabled(false);
	    		}
	    	}  
	    
	    	if(tura == false)	//jezeli tura jest false to odbieramy karte
	    	{	
	    		sr_kar.indeks = receiveRead.read();	//pobieramy karte jaka dobrac client
	    		if(sr_kar.indeks == 53)	//jezeli otrzymamy indeks 53 to znaczy ze client spasowal
	    		{
	    			encl_decyzja = 2;	//czyli jego decyzja to 2(pas)
	    			pom++;
	    		}
					
	    		if(sr_kar.indeks == 13 || sr_kar.indeks == 26 || sr_kar.indeks == 39 || sr_kar.indeks == 52)
					encl_asy++;		//aktualizujemy liczbe asow clienta
	    		if(sr_kar.indeks != 53) //jezeli client nie spasowal
	    		{
	    		for(Karta i : sr_tal.lista)
	    		{
	    			if(sr_kar.indeks == i.indeks)	//aktualizujemy liczbe punktow clienta
	    				encl_pkt = i.punkty + encl_pkt;
	    		}
	    		MainFrame.Rysuj(sr_kar.indeks);		//rysujemy odpowiedni symbol, ustawiamy wynik itd.
	    		MainFrame.UstawWyniks(encl_pkt, sr_pkt);
	    		MainFrame.JakaKartac(sr_kar.indeks);
	    		MainFrame.SprawdzWynik(encl_pkt, sr_pkt, encl_asy, sr_asy);
	    		sr_tal.UsunKarte(sr_kar.indeks);   //usuwamy karte poniewaz zostala juz dobrana przez clienta
	    		}
	    		if(sr_decyzja != 2)		//tura na true gdyz nastepnie bedziemy pobierac karte	
		    		tura = true;		//chyba ze spasowalismy
	    		else
				{
					pwrite.write(53);	//utrzymanie komunikacji
					pwrite.flush();
				}
	    		
					if(sr_decyzja == 2 && encl_decyzja == 2 && pom == 1)	//dwoch graczy spasowalo
					{
						MainFrame.SprawdzWynikPas(encl_pkt, sr_pkt, encl_asy, sr_asy);  //sprawdzamy wynik
						pom++;
					}  
	    	}
	    	
	    }
	    
	    
	    
	}
    
}

