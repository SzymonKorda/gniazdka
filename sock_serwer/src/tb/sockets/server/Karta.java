package tb.sockets.server;

public class Karta 	//karta zawiera 4 pola: kolor, figure, punkty i indeks
{					//na podstawie indeksu jestesmy wstanie powiedziec jaka dokladnie karte uzywamy
	String kolor;	//kazda karta ma inny indeks
	String figura;	//komunikacja polega glownie na ideksie karty
	int punkty;
	public int indeks;
	
	public Karta(String k, String f, int p, int i)
	{
		kolor = k;
		figura = f;
		punkty = p;
		indeks = i;
	}

}
