package tb.sockets.server;

import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.text.ParseException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;


import tb.sockets.server.Server;
import tb.sockets.server.Server;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JFormattedTextField;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.border.LineBorder;

public class MainFrame extends JFrame {

	private static JPanel contentPane;
	public static OrderPane panel;
	public static JButton sr_dobierz, sr_pasuj;;
	JButton but_zasady;
	public static JLabel clientlab, serverlab, kartlab, lblNotConnected;
	static Talia t1 = new Talia();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame(); //uruchamiamy MainFrame
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		new Thread() {					//tworzymy server i uruchamiamy rozgrywke
			public void run() {
				try {
					
					Server server = new Server(); 
					server.Rozgrywka();			
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 * @throws ParseException 
	 */
	public MainFrame() throws IOException, ParseException {    			//tworzymy okienko, dodajemy odpowiednie panele
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();	//do paneli dodajemy przyciski etykiety itp
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds((screen.width-422)/2, (screen.height-450)/2, 422, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Serwer");
		
		panel = new OrderPane();
		panel.setBounds(145, 14, 250, 350);
		contentPane.add(panel);
		
		JLabel lblNotConnected = new JLabel("Polaczony", SwingConstants.CENTER);
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 104, 123, 23);
		contentPane.add(lblNotConnected);
		
		JPanel panel1 = new JPanel();
		panel1.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		panel1.setBounds(10, 138, 123, 123);
		contentPane.add(panel1);
		panel1.setLayout(new GridLayout(4, 1, 5, 5));
		
		clientlab = new JLabel("Client: 0");
		clientlab.setBounds(10,300,70,20);
		panel1.add(clientlab);
		
		serverlab = new JLabel("Server: 0");
		serverlab.setBounds(10,350,70,20);
		panel1.add(serverlab);
		
		kartlab = new JLabel("Wylosowana Karta: ");
		kartlab.setBounds(10,370,200,20);
		contentPane.add(kartlab);
	
		
		sr_dobierz = new JButton("Dobierz");
		sr_dobierz.setBounds(100,100, 20,30);
		sr_dobierz.addActionListener(new ActionListener()  //jezeli nacisniemy przycisk dobierz i jest nasza tura
		{													//podejmujemy decyzje o dobraniu karty
															//1 oznacza decyzje o dobraniu karty
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Server.tura == true)
				{
					Server.sr_decyzja = 1;
				}
			}
			
						
		});
		panel1.add(sr_dobierz);
		
		sr_pasuj = new JButton("Pasuj");
		sr_pasuj.setBounds(100,100, 20,30);
		sr_pasuj.addActionListener(new ActionListener() //jezeli nacisniemy przycisk pasuj to pasujemy do konca gry
		{												//2 oznacza decyzje o pasowaniu

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Server.tura == true)
				{
					Server.sr_decyzja = 2;
				}
			}
			
						
		});
		panel1.add(sr_pasuj);
		
		but_zasady = new JButton("Zasady gry");		//przycisk z zasadami rozgrywki
		but_zasady.setBounds(290,370,100,20);
		but_zasady.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				JOptionPane.showMessageDialog(contentPane,"Celem gry jest uzbieranie sumy punkt�w r�wnej 21. Jezeli kt�ry� z graczy uzyska\n"
						+ " wi�cej ni� 21 pkt to przegrywa. Gracz, kt�ry zbierze 2 asy wygrywa"
						+ " bez wzgl�du \nna liczb� punkt�w. Pasowanie oznacza brak mo�liwo�ci dobierania kart do ko�ca rozgrywki"
						+ "\nJe�eli dw�ch graczy spasuje, wygrywa gracz kt�ry osi�gnie liczb� punkt�w bli�sz� 21.\n "
						+ "Remis jest mo�liwy wtedy gdy dw�jka graczy spasuje i uzyska ten sam wynik.\n"
						+ "Karty od 2-10 maja wartosci odpowiadajece wartosciom kart\n W-Walet(2pkt)"
						+ "D-Dama(3pkt), K-krol(4pkt), A-As(11pkt).");
			}
			
						
		});
		contentPane.add(but_zasady);
		
		
		
	} 
	public static void UstawWyniks(int a, int b)   //wyswietlamy obecny wynik
	{
		String s1 = String.valueOf(a);
		String s2 = String.valueOf(b);
		clientlab.setText("Client: " + s1);
		serverlab.setText("Server: " + s2);
		
	}
	
	public static void JakaKartac(int a)     //sprawdzamy jaka karte dobral client
	{
		for(Karta i : t1.lista)
		{
			if(a == i.indeks)
			{
				kartlab.setText("Client: Figura: " + i.figura + " Kolor: " + i.kolor);
			}
				
		}
	}
	
	public static void JakaKartas(int a)    //sprawdzamy jaka karte dobral server
	{
		for(Karta i : t1.lista)
		{
			if(a == i.indeks)
			{
				kartlab.setText("Server: Figura: " + i.figura + " Kolor: " + i.kolor);
			}
				
		}
	}
	
	public static void SprawdzWynik(int c, int s, int ca, int sa )  //sprawdzamy wynik rozgrywki
	{
		if(c == 21 || s == 21 || c > 21 || s > 21 || ca == 2 || sa == 2)
		{
		 if(ca == 2 || sa == 2)
		 {
			 JOptionPane.showMessageDialog(contentPane,"Remis");
			 Wylacz();
		 }
		 else  if(c == s && c == 0 )
		 {
			 JOptionPane.showMessageDialog(contentPane,"Remis! Brak zwyciezcy.");
			 Wylacz();
		 }
			
		 else  if(c == s && c == 21 && s == 21 ) 
		 {
			 JOptionPane.showMessageDialog(contentPane,"Remis! Brak zwyciezcy.");
			 Wylacz();
		 }
			 
	        else if(c == 21 && s != 21)
	        {
	        	JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	        	Wylacz();
	        }
	        	 
	        else if(s == 21 && c != 21)
	        {
	        	 JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");
	        	 Wylacz();
	        }
	        	
	        else if(c < 21 && s < 21)                   
	        {
	            if(c > s)
	            {
	            	JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	            	Wylacz();
	            }
	            	
	            else
	            {
	            	JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");
	            	Wylacz();
	            }
	        }
	        else if(c > 21 && s > 21)
	        {
	            if(c < s)
	            {
	            	JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	            	Wylacz();
	            }
	            	
	            else
	            {
	            	JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");
	            	Wylacz();
	            }
	        }
	        else if(c < 21 && s > 21)
	        {
	        	JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	        	Wylacz();
	        }
	        	
	        else if(c > 21 && s < 21)
	        {
	        	JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");
	        	Wylacz();
	        }
	        		
		
		}
	}
	
	public static void SprawdzWynikPas(int c, int s, int ca, int sa) //sprawdzamy wynik rozgrywki gdy obydwu graczy spasuje
	{
		if(c == 21 || s == 21 || c < 21 || s < 21 || ca == 2 || sa == 2)
		{
		 if(ca == 2 || sa == 2)
			 JOptionPane.showMessageDialog(contentPane,"Remis");
		 else  if(c == s) //|| ca == 2 || ca == sa
			 JOptionPane.showMessageDialog(contentPane,"Remis! Brak zwyciezcy.");
		 else  if(c == s && c == 21 ) //|| ca == 2 || ca == sa
			 JOptionPane.showMessageDialog(contentPane,"Remis! Brak zwyciezcy.");
	        else if(c == 21 && s != 21)
	        	 JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	        else if(s == 21 && c != 21)
	        	 JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");
	        else if(c < 21 && s < 21)                  
	        {
	            if(c > s)
	            	JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	            else JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");
	        }
	        else if(c > 21 && s > 21)
	        {
	            if(c < s)
	            	JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	            else JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");
	        }
	        else if(c < 21 && s > 21)
	        	JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	        else if(c > 21 && s < 21)
	        	JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");	
		}
		 
	}
	
	public static void Rysuj(int i)    //rysujemy symbol odpowiadajacy konkretnej karcie
	{
		panel.Rysuj(i);
	}
	
	public static void Wylacz()   //wylaczamy przyciski
	{
		sr_dobierz.setEnabled(false);
		sr_pasuj.setEnabled(false);
	}
	
	
}
