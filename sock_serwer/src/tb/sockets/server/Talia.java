package tb.sockets.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class Talia 
{
	List<Karta> lista = new ArrayList<>(); //talia to ArrayList typu Karta
	
	public Talia()
	{
		UtworzTalie();
	}

	public void UtworzTalie()	//tworzymy talie kart, nadajemy karta kolor, figure, wartosc punktowa oraz indeks
	{
		String [] kolory = {"kier", "karo", "trefl", "pik"};
		String [] figury = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "walet", "dama", "krol", "as"};    
		int [] wartosci= {2, 3, 4, 5, 6, 7, 8, 9, 10, 2, 3, 4, 11};                                         
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 13; j++)
			{
				Karta pom =  new Karta(kolory[i], figury[j], wartosci[j], 0);
				Dodaj(pom);
				
			}
		}
		int numer = 1;
		for(Karta k : lista)
		{
			k.indeks = numer;
			numer++;
		}
	}
	
	public void Dodaj(Karta kar)	//dodajemy do tali karte
	{
		lista.add(kar);
	}
	
	public void TasujKarty()		//tasujemy karty
	{
		Collections.shuffle(lista);
	}
	
	public Karta DobierzKarte()		//dobieramy karte na reke gracza, potem usuwamy ja z jego tali
	{
		Karta k1 = new Karta("a", "b", 1, 0);
		for(Karta i : lista)
		{
			k1 = i;
			lista.remove(i);
			break;
		}
		
		return k1;
	}
	
	public void UsunKarte(int k)	//usuniecie karty dobranej przez przeciwnika
	{
		for(Karta i : lista)
		{
			if(k == i.indeks)
			{
				lista.remove(i);
				break;
			}
			
		}
	}

}
