package tb.sockets.client;

import java.io.*;
import java.net.*;

import javax.swing.JLabel;

import tb.sockets.client.MainFrame;
import tb.sockets.client.Talia;
import tb.sockets.server.Server;
import tb.sockets.client.Karta1;

public class Client 					//client i server dzialaja niemal identycznie
{										//dokladne dzialanie opisane jest na przykladzie servera
	public Talia cl_tal =  new Talia();
	public static int cl_pkt = 0;
	public int cl_asy = 0;
	public int ensr_asy = 0;
	public static int ensr_pkt = 0;
	public Karta1 cl_kar = new Karta1("a", "b", 0, 0);
	static int cl_decyzja = 0;
	static int ensr_decyzja = 0;
	int pom = 0;
	static boolean tura = true;  //tura true gdyz client zaczyna rozgrywke
	Socket sock;
	BufferedReader keyRead;
	OutputStream ostream;
	PrintWriter pwrite;
	InputStream istream;
	static BufferedReader receiveRead;
	public Client(String ip, int port) throws Exception
	{
	
		
		cl_tal.TasujKarty();
		sock = new Socket(ip, port);
		keyRead = new BufferedReader(new InputStreamReader(System.in));
		ostream = sock.getOutputStream(); 
		pwrite = new PrintWriter(ostream, true);
		istream = sock.getInputStream();
		receiveRead = new BufferedReader(new InputStreamReader(istream));
		
	}
	 
	public void Rozgrywka() throws Exception
	{        
		
		while(true)
		{
		
			pwrite.flush();
			if(cl_decyzja == 1)	
			{
				if(tura == true)
				{
					cl_kar = cl_tal.DobierzKarte();
					cl_pkt = cl_pkt + cl_kar.punkty;
					if(cl_kar.indeks == 13 || cl_kar.indeks == 26 || cl_kar.indeks == 39 || cl_kar.indeks == 52)
						cl_asy++;
					MainFrame.Rysuj(cl_kar.indeks);
					MainFrame.UstawWynikc(cl_pkt, ensr_pkt);
					MainFrame.JakaKartac(cl_kar.indeks);
					MainFrame.SprawdzWynik(cl_pkt, ensr_pkt, cl_asy, ensr_asy);
					pwrite.write(cl_kar.indeks);
					pwrite.flush();
					if(cl_kar.indeks != 53)
					tura = false;
					cl_decyzja = 0;
				}
			}	
			
			if(cl_decyzja == 2)
	    	{
				
	    		if(tura == true)
	    		{
	    			if(cl_decyzja == 2 && ensr_decyzja == 2 && pom == 1)
					{
						MainFrame.SprawdzWynikPas(cl_pkt, ensr_pkt, cl_asy, ensr_asy);
						pom++;
					}
	    			pwrite.write(53);
	    			pwrite.flush();
	    			tura = false;
	    			MainFrame.but_dobierz.setEnabled(false);
	    			MainFrame.but_pasuj.setEnabled(false);
	    		}
	    	}  
			
			if(tura == false)
			{	
				cl_kar.indeks = receiveRead.read();
				if(cl_kar.indeks == 53 && pom == 0)
				{
					ensr_decyzja = 2;
					pom++;
				}
								
				if(cl_kar.indeks == 13 || cl_kar.indeks == 26 || cl_kar.indeks == 39 || cl_kar.indeks == 52)
					ensr_asy++;

				if(cl_kar.indeks != 53)
				{
				for(Karta1 i : cl_tal.lista)
				{
					if(cl_kar.indeks == i.indeks)
						ensr_pkt = i.punkty + ensr_pkt;	
				}
				MainFrame.Rysuj(cl_kar.indeks);
				MainFrame.UstawWynikc(cl_pkt, ensr_pkt);
				MainFrame.JakaKartas(cl_kar.indeks);
				MainFrame.SprawdzWynik(cl_pkt, ensr_pkt, cl_asy, ensr_asy);
				cl_tal.UsunKarte(cl_kar.indeks);
				}
				if(cl_decyzja != 2)
					tura = true;
				else
				{	
					pwrite.write(53);
					pwrite.flush();
				}
				
				
				if(cl_decyzja == 2 && ensr_decyzja == 2 && pom == 1)
				{
					MainFrame.SprawdzWynikPas(cl_pkt, ensr_pkt, cl_asy, ensr_asy);
					pom++;
				}  
						
			}
					
		}
	}
}