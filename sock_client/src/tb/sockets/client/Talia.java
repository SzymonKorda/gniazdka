package tb.sockets.client;

import java.util.ArrayList;
import java.util.List;

import tb.sockets.server.Karta;

import java.util.Collections;

public class Talia 
{
	List<Karta1> lista = new ArrayList<>();
	
	public Talia()
	{
		UtworzTalie();
	}

	public void UtworzTalie()
	{
		String [] kolory = {"kier", "karo", "trefl", "pik"};
		String [] figury = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "walet", "dama", "krol", "as"};    
		int [] wartosci= {2, 3, 4, 5, 6, 7, 8, 9, 10, 2, 3, 4, 11};                                          
		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 13; j++)
			{
				Karta1 pom =  new Karta1(kolory[i], figury[j], wartosci[j], 0);
				Dodaj(pom);
				
			}
		}
		int numer = 1;
		for(Karta1 k : lista)
		{
			k.indeks = numer;
			numer++;
		}
	}
	
	public void Dodaj(Karta1 kar)
	{
		lista.add(kar);
	}
	
	public void TasujKarty()
	{
		Collections.shuffle(lista);
	}
	
	public Karta1 DobierzKarte()
	{
		Karta1 k1 = new Karta1("a", "b", 1, 0);
		for(Karta1 i : lista)
		{
			k1 = i;
			lista.remove(i);
			break;
		}
		
		return k1;
	}
	
	public void UsunKarte(int k)
	{
		for(Karta1 i : lista)
		{
			if(k == i.indeks)
			{
				lista.remove(i);
				break;
			}
			
		}
	}
	
	

}