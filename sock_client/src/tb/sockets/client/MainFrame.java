package tb.sockets.client;

import java.awt.EventQueue;
import java.awt.Graphics2D;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import tb.sockets.server.Server;
import tb.sockets.client.OrderPane;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;

import javax.swing.JOptionPane;

import javax.swing.border.LineBorder;
import javax.swing.text.MaskFormatter;

public class MainFrame extends JFrame {

	private static JPanel contentPane;
	public static OrderPane panel;
	public static JButton but_dobierz;
	public static JButton but_pasuj;
	JButton but_zasady;
	public static JLabel serverlab;
	public static JLabel clientlab;
	public static JLabel kartlab;
	public static JLabel lblNotConnected;
	static JFormattedTextField frmtdtxtfldXxxx;  //pole tekstowe do wpisania portu
	static JFormattedTextField frmtdtxtfldIp;	//pole tekstowe do wpisania ip
	static int port;							//port i ip za pomoca ktorych laczymy sie z serverem
	public String host;
	static Talia t1 = new Talia();
	
	
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 * @throws ParseException 
	 */
	public MainFrame() throws IOException, ParseException {
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds((screen.width-422)/2, (screen.height-450)/2, 422, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Klient");
		
		panel = new OrderPane();
		panel.setBounds(145, 14, 250, 350);
		contentPane.add(panel);
		
		JLabel lblNotConnected = new JLabel("Nie polaczony", SwingConstants.CENTER);
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 104, 123, 23);
		contentPane.add(lblNotConnected);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		panel_1.setBounds(10, 138, 123, 123);
		contentPane.add(panel_1);
		panel_1.setLayout(new GridLayout(4, 1, 5, 5));
		
		clientlab = new JLabel("Client: 0");
		clientlab.setBounds(10,300,70,20);
		panel_1.add(clientlab);
		
		serverlab = new JLabel("Server: 0");
		serverlab.setBounds(10,350,70,20);
		panel_1.add(serverlab);
		
		kartlab = new JLabel("Wylosowana karta: ");
		kartlab.setBounds(10,370,200,20);
		contentPane.add(kartlab);
		
		JLabel lblHost = new JLabel("Host: ");
		lblHost.setBounds(10, 10, 35, 14);
		contentPane.add(lblHost);
		
		
		JFormattedTextField frmtdtxtfldXxxx = new JFormattedTextField();
		frmtdtxtfldXxxx.setText("xxxx");
		frmtdtxtfldXxxx.setBounds(45, 39, 90, 20);
		contentPane.add(frmtdtxtfldXxxx);
		
		
		try {
				frmtdtxtfldIp = new JFormattedTextField(new MaskFormatter("###.###.###.###"));
				frmtdtxtfldIp.setBounds(45, 11, 90, 20);
				frmtdtxtfldIp.setText("xxx.xxx.xxx.xxx");
				contentPane.add(frmtdtxtfldIp);
		} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
				
		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 40, 35, 14);
		contentPane.add(lblPort);
		
		JButton btnConnect = new JButton("Polacz");
		btnConnect.setBounds(10, 70, 90, 23);
		contentPane.add(btnConnect);
		
		
		btnConnect.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				
				new Thread() {				//laczenie zaczynamy dopiero po wprowadzeniu przez uzytkownika
					public void run() {		//poprawnego portu i adresu IP
						try {
							host = frmtdtxtfldIp.getText();
							port = Integer.valueOf(frmtdtxtfldXxxx.getText());
							Client client = new Client(host,port);
							lblNotConnected.setText("Polaczony");
							client.Rozgrywka();
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}.start();
				
			}
			
						
		});
		
		
		but_dobierz = new JButton("Dobierz");
		but_dobierz.setBounds(100,100, 20,30);
		but_dobierz.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Client.tura == true)
				{
					Client.cl_decyzja = 1;
				}
			}
			
						
		});
		panel_1.add(but_dobierz);	
		
		but_pasuj = new JButton("Pasuj");
		but_pasuj.setBounds(10,100, 20,30);
		but_pasuj.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(Client.tura == true)
				{
					Client.cl_decyzja = 2;
				}
			}
			
						
		});
		panel_1.add(but_pasuj);
		
		but_zasady = new JButton("Zasady gry");
		but_zasady.setBounds(290,370,100,20);
		but_zasady.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e) 
			{
				JOptionPane.showMessageDialog(contentPane,"Celem gry jest uzbieranie sumy punkt�w r�wnej 21. Jezeli kt�ry� z graczy uzyska\n"
						+ " wi�cej ni� 21 pkt to przegrywa. Gracz, kt�ry zbierze 2 asy wygrywa"
						+ " bez wzgl�du \nna liczb� punkt�w. Pasowanie oznacza brak mo�liwo�ci dobierania kart do ko�ca rozgrywki"
						+ "\nJe�eli dw�ch graczy spasuje, wygrywa gracz kt�ry osi�gnie liczb� punkt�w bli�sz� 21.\n "
						+ "Remis jest mo�liwy wtedy gdy dw�jka graczy spasuje i uzyska ten sam wynik.\n"
						+ "Karty od 2-10 maja wartosci odpowiadajece wartosciom kart\n W-Walet(2pkt)"
						+ "D-Dama(3pkt), K-krol(4pkt), A-As(11pkt).");
			}
			
						
		});
		contentPane.add(but_zasady);
		
		
		} 
	public static void UstawWynikc(int a, int b)
	{
		String s1 = String.valueOf(a);
		String s2 = String.valueOf(b);
		clientlab.setText("Client: " + s1);
		serverlab.setText("Server: " + s2);
	}
	
	public static void JakaKartac(int a)
	{
		for(Karta1 i : t1.lista)
		{
			if(a == i.indeks)
			{
				kartlab.setText("Client: Figura: " + i.figura + " Kolor: " + i.kolor);
			}
				
		}
	}
	
	public static void JakaKartas(int a)
	{
		for(Karta1 i : t1.lista)
		{
			if(a == i.indeks)
			{
				kartlab.setText("Server: Figura: " + i.figura + " Kolor: " + i.kolor);
			}
				
		}
	}
	
	public static void SprawdzWynik(int c, int s, int ca, int sa )
	{
		if(c == 21 || s == 21 || c > 21 || s > 21 || ca == 2 || sa == 2)
		{
		 if(ca == 2 || sa == 2)
		 {
			 JOptionPane.showMessageDialog(contentPane,"Remis");
			 Wylacz();
		 }
		 else  if(c == s && c == 0 )
		 {
			 JOptionPane.showMessageDialog(contentPane,"Remis! Brak zwyciezcy.");
			 Wylacz();
		 }
			
		 else  if(c == s && c == 21 && s == 21 ) 
		 {
			 JOptionPane.showMessageDialog(contentPane,"Remis! Brak zwyciezcy.");
			 Wylacz();
		 }
			 
	        else if(c == 21 && s != 21)
	        {
	        	JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	        	Wylacz();
	        }
	        	 
	        else if(s == 21 && c != 21)
	        {
	        	 JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");
	        	 Wylacz();
	        }
	        	
	        else if(c < 21 && s < 21)                  
	        {
	            if(c > s)
	            {
	            	JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	            	Wylacz();
	            }
	            	
	            else
	            {
	            	JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");
	            	Wylacz();
	            }
	        }
	        else if(c > 21 && s > 21)
	        {
	            if(c < s)
	            {
	            	JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	            	Wylacz();
	            }
	            	
	            else
	            {
	            	JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");
	            	Wylacz();
	            }
	        }
	        else if(c < 21 && s > 21)
	        {
	        	JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	        	Wylacz();
	        }
	        	
	        else if(c > 21 && s < 21)
	        {
	        	JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");
	        	Wylacz();
	        }
	        		
		
		}
	}
	
	public static void SprawdzWynikPas(int c, int s, int ca, int sa)
	{
		if(c == 21 || s == 21 || c < 21 || s < 21 || ca == 2 || sa == 2)
		{
		 if(ca == 2 || sa == 2)
			 JOptionPane.showMessageDialog(contentPane,"Remis");
		 else  if(c == s ) 
			 JOptionPane.showMessageDialog(contentPane,"Remis! Brak zwyciezcy.");
	        else if(c == 21 && s != 21)
	        	 JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	        else if(s == 21 && c != 21)
	        	 JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");
	        else if(c < 21 && s < 21)                   
	        {
	            if(c > s)
	            	JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	            else JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");
	        }
	        else if(c > 21 && s > 21)
	        {
	            if(c < s)
	            	JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	            else JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");
	        }
	        else if(c < 21 && s > 21)
	        	JOptionPane.showMessageDialog(contentPane,"Client wygrywa!");
	        else if(c > 21 && s < 21)
	        	JOptionPane.showMessageDialog(contentPane,"Server wygrywa!");	
		}
		 
	}
	
	public static void Rysuj(int i)
	{
		panel.Rysuj(i);
	}
	
	public static void Wylacz()
	{
		but_dobierz.setEnabled(false);
		but_pasuj.setEnabled(false);
	}
	
	
	
}
